import re
import sys
import nltk
import requests
import json
from bs4 import BeautifulSoup

# ADD THIS
# nltk.pos_tag(text)

class PunGenerator:
    corpus = None

    # Constructor
    def __init__(self):
        #Read the common corpus json file into dictionary
        self.corpus = self.build_corpus()


    def generate_puns(self, word):

        try:
            # Attempt to find the users word in our dictionary
            pronunciation = self.corpus[word]

            # Find another word in the corpus that has the closest pronunciation
            match = self.find_match(word, pronunciation, self.corpus).lower()

            # Scrape the web for a sentence with the match word in it
            found_sentences = self.scrape_your_dictionary(match)

            if found_sentences is not None:
                return (self.replace_word(found_sentences, match, word.lower()))
            else:
                print("No sentences found for {}".format(match))

        except KeyError:
            print("Corpus does not contain a match for the word {}".format(word))



    def build_corpus(self):
        """ Reads in corpus from JSON file and store in dictionary """

        with open("big_common_corpus.json", 'r') as file:
            corpus = json.load(file)
        return corpus



    def find_match(self, word, pronunciation, corpus):
        """ Find a word from the corpus with the most similar pronunciation to the input word"""

        min_edit_dist = sys.maxsize
        pronunciation = ' '.join(pronunciation)

        for word_key in corpus:
            match_pronunciation = ' '.join(corpus[word_key])

            # Optimization 1 (see below for details)                Optimization 2 (see below for details)
            if( ((len(word_key) - min_edit_dist) < len(word)) and ((len(word_key) + min_edit_dist) > len(word)) ):

                ed = self.edit_distance(pronunciation, match_pronunciation)
                
                if (ed < min_edit_dist and pronunciation != match_pronunciation and not self._arePlurals(word, word_key)):
                    min_edit_dist = ed
                    closest_string = word_key

        print("\n\nThe closest match for {} is: {}\nThe edit distance is: {}".format(word, closest_string, min_edit_dist))


        return closest_string
        

    def edit_distance(self, str1, str2):
        """ Computes edit distance between two strings str1 and str2. """

        n = len(str1)
        m = len(str2)

        dp = [[True for i in range(n+1)] for i in range(m+1)]

        for i in range(m+1):
            for j in range(n+1):
                if i == 0:
                    dp[i][j] = j
                elif j == 0:
                    dp[i][j] = i
                elif str1[j-1] == str2[i-1]:
                    dp[i][j] = dp[i-1][j-1]
                else:
                    dp[i][j] = 1 + min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1])

        return dp[m][n]



    def _arePlurals(self, str1, str2):
        if(str1 + 'S' == str2 or str1 == str2 + "S"):
            return True
        return False
        

    def scrape_your_dictionary(self, match):
        url = "https://sentence.yourdictionary.com/{}".format(match)
        sentence_list = list()

        try:
            response = requests.get(url)
        except:
            return None
        
        if(response):
            soup = BeautifulSoup(response.text, 'html.parser')
            sentence_components = soup.findAll(class_='sentence component')


            for sentence in sentence_components:
                sentence_list.append(sentence.find('p').text)

        return sentence_list


    def replace_word(self, found_sentences, match, word):
        pun_sentences = list()

        for sentence in found_sentences:
            if match in sentence:
                pun_sentences.append(sentence.replace(match, word))

        return pun_sentences






"""
Search pruning optimizations:

1) If length of a potential match string - min_edit_dist is >= length of original string, 
dont compute edit disnce since it cannot be less

Ex/ original string == TEST
    min_edit_dist == 3
    potential macth = TESTIII

    This is the best case where it is the same string ( + 0 edit distance) with 3 of any letter at the end.
    The edit distance must be at least 3 since we have to delete those 3 letters 


2) If length of a potential match string + min_edit_dist is <= length of original string, 
dont compute edit disnce since it cannot be less

Ex/ original string == COMPUTE
    min_edit_dist == 3
    potential macth = COMP

    This is the best case where it is the same string ( + 0 edit distance) without the last 43letters at the end.
    The edit distance must be at least 3 since we have to add those 3 letters 

    


ISSUES:
-If there are no more puns, it should not prompt you to choose for more

TIME TEST:

-No Optimizations
    MAGIC: 13.849 seconds
    ORGASM: 12.535 seconds
    CHICKEN: 11.117 seconds
    AUTOMOBILE: 26.465 seconds
    GOVERNMENT: 14.586 seconds
    CHARACTERISTIC: 22.389 seconds
    INSTRUMENTS: 17.337 seconds

-Optimization 1
    MAGIC: 4.153 seconds
    ORGASM: 7.380 seconds
    CHICKEN: 5.399 seconds
    AUTOMOBILE: 15.497 seconds
    GOVERNMENT: 12.032 seconds
    CHARACTERISTIC: 18.858 seconds
    INSTRUMENTS: 15.619 seconds

-Optimization 1 & 2
    MAGIC: 4.38 seconds
    ORGASM: 6.542 seconds
    CHICKEN: 3.906 seconds
    AUTOMOBILE: 15.203 seconds
    GOVERNMENT: 6.55 seconds
    CHARACTERISTIC: 11.011 seconds
    INSTRUMENTS: 14.223 seconds


Future:
-use second closest match for a pun if first is bad
-use part of speech

"""
