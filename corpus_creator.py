from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import json
import nltk

stop_words = set(stopwords.words("english"))
arpabet = nltk.corpus.cmudict.dict()

# test_words=['sandip', 'inanition', 'heartstrings', 'centralisation']
# for word in test_words:
# 	try:
# 		print(arpabet[word])
# 	except:
# 		print("NOOOO: {}".format(word))


# Create a list of 7000 most common words from file
with open("100k_words.txt", "r") as w_file:
    words = w_file.readlines()

corpus = {}

for word in words:
	if word not in stop_words:
		word = word.rstrip('\n').lower()
		try:
			corpus[word.upper()] = arpabet[word][0]
		except KeyError:
			print("Couldnt find word: {}".format(word))

# Write the dictionary to the file
with open('big_common_corpus.json', 'w') as outfile:
    json.dump(corpus, outfile)

